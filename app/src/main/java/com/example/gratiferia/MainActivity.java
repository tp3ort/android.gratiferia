package com.example.gratiferia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.gratiferia.Helpers.Sesion;


public class MainActivity extends AppCompatActivity {

    Button btnRegistro;
    Button btnLogin;
    Button btnPubli;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

      /*  final Sesion sesion = new Sesion(getApplicationContext());

        if (sesion.getSesion() != ""){
            Intent intent = new Intent(MainActivity.this, MenuPrincipalActivity.class);
            startActivity(intent);
            finish();
        }*/


        btnRegistro = findViewById(R.id.btn_registro);
        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RegistroActivity.class);
                startActivity(intent);

            }
        });

        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               Intent intent = new Intent(MainActivity.this, LoginActivity.class);
               startActivity(intent);
               finish();
                // ******** Solo para probar Menú Principal
              //  Intent intent = new Intent(MainActivity.this, MenuPrincipalActivity.class);
              //  startActivity(intent);
                // *****************************************

            }
        });

        btnPubli = findViewById(R.id.btn_publi);
        btnPubli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ConsumirPublicacionesActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
