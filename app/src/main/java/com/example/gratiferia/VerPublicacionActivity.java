package com.example.gratiferia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class VerPublicacionActivity extends AppCompatActivity {

    Intent intent;
    Bundle bundle;

    TextView titulo, descripcion, categoria, estado;
    String latitud, longitud;
    ImageView imagen;
    ProgressBar progressBar;

    ImageView btnLocalizacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_publicacion);


        progressBar = findViewById(R.id.progressBarImagen);
        progressBar.setVisibility(View.VISIBLE);



        intent = getIntent();
        bundle = intent.getExtras();

        setViews();

        populateView(intent, bundle);

        btnLocalizacion = findViewById(R.id.btnLocalizacion);

        btnLocalizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String url = "https://www.google.com.ar/maps/";
                if (latitud != null && longitud != null) {
                    url = "https://www.google.com.ar/maps/@" + latitud + "," + longitud + ",15z";
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                }else{
                    Toast.makeText(getApplicationContext(), "No es posible localizarlo", Toast.LENGTH_LONG).show();
                }


            }
        });

    }

    private void populateView(Intent intent, Bundle bundle) {

        titulo.setText(intent.getExtras().get("titulo").toString());

        descripcion.setText(intent.getExtras().get("descripcion").toString());

        categoria.setText(intent.getExtras().get("categoria").toString());

        estado.setText(intent.getExtras().get("estado").toString());
        if (intent.getExtras().get("zonalat") != null){
            latitud = intent.getExtras().get("zonalat").toString();
        }if(intent.getExtras().get("zonalong") != null){
            longitud = intent.getExtras().get("zonalong").toString();
        }


        //imagen.setImageBitmap((Bitmap)bundle.getParcelable("imagen"));

        Picasso.get().load(intent.getExtras().get("imagen").toString())
                .rotate(90).into(imagen, new Callback() {
            @Override
            public void onSuccess() {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {
                Toast.makeText( VerPublicacionActivity.this, "La imagen no se puede cargar", Toast.LENGTH_LONG).show();
            }
        });


    }

    private void setViews() {

        titulo = findViewById(R.id.txtVerTitulo);
        descripcion = findViewById(R.id.txtVerDescripcion);
        categoria = findViewById(R.id.txtVerCategoria);
        estado = findViewById(R.id.txtEstado);
        imagen = findViewById(R.id.imgVerPublicacion);

    }
}
