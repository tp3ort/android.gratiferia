package com.example.gratiferia;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.gratiferia.Helpers.LocalizacionService;
import com.example.gratiferia.Helpers.Publicacion;
import com.example.gratiferia.Helpers.PublicacionesService;
import com.example.gratiferia.Helpers.RetrofitService;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.zip.Inflater;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LocalizacionDialog extends AppCompatDialogFragment {

    private EditText calle, altura, localidad;
    private LocalizacionDialogListener listener;
    private Button buscar;
    private Coordenada _coordenada;
    private String _return;

    private TextView sinResultados;

    ListView listaResultados;
    ArrayList<String> resultados = new ArrayList<>();
    ArrayList<Coordenada> coordenadas = new ArrayList<>();

    ArrayAdapter arrayAdapter;

    public class Coordenada{
        String latitud;
        String longitud;
        public Coordenada(String latitud, String longitud){
            this.latitud = latitud;
            this.longitud = longitud;
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());



        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.localizacion, null);

        sinResultados = view.findViewById(R.id.txtSinResultado);

        arrayAdapter = new ArrayAdapter(view.getContext(), android.R.layout.simple_list_item_1, resultados);
        listaResultados = view.findViewById(R.id.listaResultados);
        listaResultados.setAdapter(arrayAdapter);


        buscar = view.findViewById(R.id.btnBuscarLocalizacion);

        buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                resultados.clear();
                coordenadas.clear();

                if(validTexts()){

                try{
                    String sCalle, sAltura, sLocalidad;

                    sCalle = calle.getText().toString().trim();
                    sAltura = altura.getText().toString().trim();
                    sLocalidad = localidad.getText().toString().trim();
/*                    if (sLocalidad.equals("")){
                        sLocalidad = "Argentina";
                    }*/

                    Retrofit retrofit = RetrofitService.hereApi();

                    LocalizacionService locService = retrofit.create(LocalizacionService.class);

                    Call<JsonObject> call;

                    call = locService.getLocalizaciones(
                            sAltura,sCalle,sLocalidad,"Argentina","9",
                            "zzrQ6AkdbYl6LJImUGUA",
                            "qeYleSqv6JaexQtZ_dcQLQ");

                    call.enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            JsonArray resp = response.body().get("Response")
                                    .getAsJsonObject().get("View")
                                    .getAsJsonArray();

                             if(resp.size()==0){
                                 sinResultados.setVisibility(View.VISIBLE);
                             }else{
                                 sinResultados.setVisibility(View.GONE);
                                 resp = resp.get(0).getAsJsonObject()
                                         .get("Result").getAsJsonArray();
                             }

                            for (JsonElement jo : resp) {
                                String location = jo.getAsJsonObject().get("Location").getAsJsonObject()
                                        .get("Address").getAsJsonObject()
                                        .get("Label").getAsString();

                                String _lat, _long;
                                _lat = jo.getAsJsonObject().get("Location").getAsJsonObject()
                                        .get("DisplayPosition").getAsJsonObject()
                                        .get("Latitude").getAsString();

                                _long = jo.getAsJsonObject().get("Location").getAsJsonObject()
                                        .get("DisplayPosition").getAsJsonObject()
                                        .get("Longitude").getAsString();

                                resultados.add(location);
                                coordenadas.add(new Coordenada(_lat, _long));



                            }



                            arrayAdapter.notifyDataSetChanged();

                            Log.d("alo", "alo");

                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {
                            Log.d("alo", "alo");
                        }
                    });

                }catch (Exception e){

                }

                }


            }
        });


        builder.setView(view)
                .setTitle("Zona");


        calle = view.findViewById(R.id.calle);
        altura = view.findViewById(R.id.altura);
        localidad = view.findViewById(R.id.localidad);


        listaResultados.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                   @Override
                                                   public void onItemClick (AdapterView < ? > adapter, View view,int position, long arg){
                                                       // TODO Auto-generated method stub
                                                       if (getDialog().isShowing()){
                                                           getDialog().dismiss();
                                                       }

                                                         _return = resultados.get(position);
                                                        _coordenada = coordenadas.get(position);
                                                        listener.aplicarCampos(_return, _coordenada);
                                                   }
                                               }
        );


        return builder.create();


    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        try {
            listener = (LocalizacionDialogListener) context;
        }catch(ClassCastException e){
            throw new ClassCastException(context.toString());
        }

    }

    private boolean validTexts() {

        if(calle.getText().toString().trim().equals("")){
            calle.setError("Debe completar el nombre de la calle");
            return false;
        }else if(altura.getText().toString().trim().equals("")){
            altura.setError("Debe ingresar alguna altura aproximada");
            return false;
        }

        return true;
    }

    public interface LocalizacionDialogListener{
        void aplicarCampos(String returned, Coordenada coordenada);


    }
}
