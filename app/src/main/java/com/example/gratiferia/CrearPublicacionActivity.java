package com.example.gratiferia;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.gratiferia.Helpers.Publicacion;
import com.example.gratiferia.Helpers.PublicacionesService;
import com.example.gratiferia.Helpers.RetrofitService;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class CrearPublicacionActivity extends AppCompatActivity implements OnItemSelectedListener, LocalizacionDialog.LocalizacionDialogListener {

    SharedPreferences pref;

    ProgressDialog progreso;

    LocalizacionDialog.Coordenada coordenada;

    Button btnTomarFoto, btnSeleccionar, btnGuardar;

    ImageView imgV;

    EditText txtTitulo, txtDescripcion, txtZona, txtZonaLat, txtZonaLong, txtPalabrasClaves;

    Uri photoUri;

    boolean fotoCargada;

    Spinner spn;

    String categoria;

    FirebaseStorage firebaseStorage;
    StorageReference ref;

    JsonObject jsonObject;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_publicacion);

        setCampos();


        txtZona.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b){

                        LocalizacionDialog localizacionDialog = new LocalizacionDialog();
                        localizacionDialog.show(getSupportFragmentManager(), "dialog");

                        view.clearFocus();
                    }

            }
        });



        pref = getSharedPreferences("user_details",MODE_PRIVATE);

        firebaseStorage = FirebaseStorage.getInstance();

        ref = firebaseStorage.getReference().child("images/"+ UUID.randomUUID());





        spn = findViewById(R.id.spnCategoria);
        spn.setOnItemSelectedListener(this);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.categoria_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spn.setAdapter(adapter);


        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progreso = new ProgressDialog(CrearPublicacionActivity.this);
                progreso.setMessage("Creado Publicacion...");
                progreso.setTitle("Publicación");
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);

                try {

                    jsonObject = getJsonFromView();

                    validarCampos();


                    progreso.show();
                    progreso.setCancelable(false);

                    Bitmap resizes = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), photoUri);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    resizes.compress(Bitmap.CompressFormat.JPEG, 20, baos);
                    byte[] fileInBytes = baos.toByteArray();
                    ref.putBytes(fileInBytes)
                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {


                                            jsonObject.addProperty("image", uri.toString() );

                                            Retrofit retrofit = RetrofitService.retrofit();

                                            PublicacionesService jsonPlaceHolderApi = retrofit.create(PublicacionesService.class);
                                            Call<Publicacion> call = jsonPlaceHolderApi.createPublicacion(jsonObject);

                                            call.enqueue(new Callback<Publicacion>() {
                                                @Override
                                                public void onResponse(Call<Publicacion> call, Response<Publicacion> response) {
                                                    progreso.dismiss();
                                                    limpiarCampos(spn);
                                                    Toast.makeText(CrearPublicacionActivity.this, "Publicación creada", Toast.LENGTH_LONG).show();
                                                }

                                                @Override
                                                public void onFailure(Call<Publicacion> call, Throwable t) {
                                                    progreso.dismiss();
                                                    Toast.makeText(CrearPublicacionActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();

                                                }
                                            });


                                        }
                                    });



                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            progreso.dismiss();
                            Toast.makeText(CrearPublicacionActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();

                        }
                    });
                }catch(Exception e){
                    progreso.dismiss();
                    Toast.makeText(CrearPublicacionActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();

                }



            }
        });

        btnTomarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pedirPermisos(Manifest.permission.CAMERA, CAMARA_REQUEST);

            }
        });

        btnSeleccionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pedirPermisos(Manifest.permission.READ_EXTERNAL_STORAGE, STORAGE_REQUEST);
            }
        });


    }

    private void dispatchSelectPictureIntent() {

        Intent selectPictureIntent = new Intent(Intent.ACTION_PICK);

        if (selectPictureIntent.resolveActivity(getPackageManager()) != null) {

                    selectPictureIntent.setType("image/*");
                    startActivityForResult(selectPictureIntent, RESULT_LOAD_IMG);
        }

    }

    static final int CAMARA_REQUEST = 1;
    static final int STORAGE_REQUEST = 2;

    private void pedirPermisos(String tipoPermiso, int tipoPeticion){
        if (ContextCompat.checkSelfPermission(CrearPublicacionActivity.this,
                tipoPermiso)
                != PackageManager.PERMISSION_GRANTED) {


                ActivityCompat.requestPermissions(CrearPublicacionActivity.this,
                        new String[]{tipoPermiso},
                        tipoPeticion);

        }else{
            switch (tipoPeticion){
                case CAMARA_REQUEST:
                    dispatchTakePictureIntent();
                    break;
                case STORAGE_REQUEST:
                    dispatchSelectPictureIntent();
                    break;

            }

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {


        switch (requestCode){

            case CAMARA_REQUEST:

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchTakePictureIntent();
                }else{
                    new AlertDialog.Builder(this)
                            .setTitle("Permisos Requeridos")
                            .setMessage("No podrás crear publicaciones con fotos tomadas desde tu cámara")
                            .setPositiveButton("Entendido", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            })
                            .create().show();

                }


            break;

            case STORAGE_REQUEST:

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchSelectPictureIntent();
                }else{
                    new AlertDialog.Builder(this)
                            .setTitle("Permisos Requeridos")
                            .setMessage("No podrás crear publicaciones con fotos de tu galeria")
                            .setPositiveButton("Entendido", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            })
                            .create().show();
                }

                break;

        }



    }


    private void limpiarCampos(Spinner spn) {
        txtZona.setText("");

        txtDescripcion.setText("");
        txtTitulo.setText("");
        txtPalabrasClaves.setText("");
        spn.setSelection(0);
        Drawable im = getResources().getDrawable(R.drawable.iconnapp);
        imgV.setImageDrawable(im);
    }

    private JsonObject getJsonFromView() {

        JsonObject jo = new JsonObject();

        jo.addProperty("title", txtTitulo.getText().toString());

        jo.addProperty("description", txtDescripcion.getText().toString());

        jo.addProperty("category", categoria);

        jo.addProperty("zone", txtZona.getText().toString());

        jo.addProperty("zonelat", coordenada.latitud);

        jo.addProperty("zonelong", coordenada.longitud);

        jo.addProperty("keyword", txtPalabrasClaves.getText().toString());

        jo.addProperty("state","available");

        jo.addProperty("owner", pref.getString("id", null) );

        jo.addProperty("reservedby", "null");


        return jo;
    }


    private void validarCampos(){

        if (txtTitulo.getText().toString().trim().equals("")){
            throw new IllegalArgumentException ("Debe indicar un titulo para la publicación");
        }else if (categoria.equals("Seleccione una Categoria")){
            throw new IllegalArgumentException ("Debe seleccionar una categoria");
        }else if (txtDescripcion.getText().toString().trim().equals("")){
            throw new IllegalArgumentException ("Debe indicar Descripción");
        }else if (txtZona.getText().toString().trim().equals("")){
            throw new IllegalArgumentException ("Debe indicar Zona de retiro");
        }else if (txtPalabrasClaves.getText().toString().trim().equals("")){
            throw new IllegalArgumentException ("Debe una palabra clave");
        }else if(fotoCargada == false){
            throw new IllegalArgumentException ("Debe seleccionar o tomar una foto");
        }

    }

    private void setCampos() {


        btnTomarFoto = findViewById(R.id.btnTomarFoto);
        btnSeleccionar = findViewById(R.id.btnSelArchivo2);
        btnGuardar = findViewById(R.id.btnGuardarPublic);

        txtZona = findViewById(R.id.txtZonaRetiro);
        txtDescripcion = findViewById(R.id.txtDescripcionPubli);
        txtTitulo = findViewById(R.id.txtTitulo);
        txtPalabrasClaves = findViewById(R.id.txtPalabrasClaves);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {

        categoria = parent.getItemAtPosition(pos).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    static final int REQUEST_TAKE_PHOTO = 1;
    static final int RESULT_LOAD_IMG = 2;

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(CrearPublicacionActivity.this,ex.getMessage(), Toast.LENGTH_LONG);
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                try{
                    String h = getApplicationContext().getPackageName();
                    photoUri = FileProvider.getUriForFile(this,
                               "com.example.gratiferia.provider",photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                    startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                }catch(Exception e){
                    Log.d("Error", e.getMessage());
                }

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        imgV = findViewById(R.id.imgImagen);

        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {


            try{
                imgV.setImageURI(photoUri);
                fotoCargada = true;
            }catch (Exception e){
                fotoCargada=false;
                throw e;
            }


        }else if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK){
            try {

                photoUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(photoUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                imgV.setImageBitmap(selectedImage);
                fotoCargada = true;

            } catch (FileNotFoundException exc) {
                fotoCargada=false;
                Toast.makeText(CrearPublicacionActivity.this, "Algo salió mal", Toast.LENGTH_LONG).show();

            }
        }
    }



    private File createImageFile() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );


        return image;
    }

    @Override
    public void aplicarCampos(String returned, LocalizacionDialog.Coordenada coordenada) {
            txtZona.setText(returned);
            this.coordenada = coordenada;
    }
}
