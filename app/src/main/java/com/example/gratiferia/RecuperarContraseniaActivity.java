package com.example.gratiferia;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.gratiferia.Helpers.MailSender;
import com.example.gratiferia.Helpers.Usuario;
import com.example.gratiferia.Helpers.UsuarioService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.mailjet.client.errors.MailjetException;
import com.mailjet.client.errors.MailjetSocketTimeoutException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RecuperarContraseniaActivity extends AppCompatActivity {

    Button btnRecuperar;
    ProgressDialog progreso;

    EditText txtEmail;
    Usuario usuario;
    Retrofit retrofit;
    Gson gson;

    UsuarioService jsonPlaceHolderApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperar_contrasenia);


        btnRecuperar = findViewById(R.id.btnRecPass);
        txtEmail = findViewById(R.id.txtRecEmail);





        btnRecuperar.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {

                try {

                    progreso = new ProgressDialog(RecuperarContraseniaActivity.this);
                    progreso.setMessage("Validando...");
                    progreso.setTitle("Recuperar Contraseña");
                    progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);


                    gson = new GsonBuilder()
                            .setLenient()
                            .create();

                    retrofit = new Retrofit.Builder().
                            baseUrl("https://apirestgratiferia.herokuapp.com/api/").addConverterFactory(GsonConverterFactory.create(gson)).
                            build();

                    jsonPlaceHolderApi = retrofit.create(UsuarioService.class);
                    Call<Usuario> call = jsonPlaceHolderApi.getUsuario(txtEmail.getText().toString());
                    progreso.show();
                    progreso.setCancelable(false);
                    call.enqueue(new Callback<Usuario>() {
                        @Override
                        public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                            if (response.code() == 400){
                                progreso.dismiss();
                                Toast.makeText(getApplicationContext(),"El email es inválido", Toast.LENGTH_LONG).show();
                            }else if (response.code() == 404){
                                progreso.dismiss();
                                Toast.makeText(getApplicationContext(),"El email ingresado no existe", Toast.LENGTH_LONG).show();
                            }
                            else{

                                progreso.dismiss();
                                usuario = response.body();

                                progreso = new ProgressDialog(RecuperarContraseniaActivity.this);
                                progreso.setMessage("Registrando solicitud...");
                                progreso.setTitle("Recuperar Contraseña");
                                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);


                                usuario.setContrasenia(contraseniaRandom(10));
                                JsonObject usuarioJson = getJsonFromObject(usuario);


                                Call<Usuario> call2 = jsonPlaceHolderApi.updateUsuario(usuario.getId(), usuarioJson);
                                progreso.show();
                                progreso.setCancelable(false);
                                call2.enqueue(new Callback<Usuario>() {
                                    @Override
                                    public void onResponse(Call<Usuario> call, Response<Usuario> response) {

                                        if (response.code() == 400){


                                            progreso.dismiss();

                                            try {
                                                JSONObject jObjError = new JSONObject(response.errorBody().string());
                                                Toast.makeText(getApplicationContext(), jObjError.getString("descripcion") ,Toast.LENGTH_LONG).show();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }

                                        }
                                        else{

                                            if (response.isSuccessful()){
                                                progreso.dismiss();


                                                Toast.makeText(getApplicationContext(), "Recibirá un email con su nueva contraseña", Toast.LENGTH_LONG).show();
                                                new Thread(new Runnable()
                                                {
                                                    @Override
                                                    public void run()
                                                    {
                                                        try
                                                        {
                                                            MailSender.sendMailRecupero(usuario);
                                                            Thread.sleep(2000);
                                                            finish();
                                                        }
                                                        catch (InterruptedException e)
                                                        {
                                                            // TODO Auto-generated catch block
                                                            e.printStackTrace();
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        } catch (MailjetSocketTimeoutException e) {
                                                            e.printStackTrace();
                                                        } catch (MailjetException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                }).start();

                                            }else{
                                                progreso.dismiss();
                                                Toast.makeText(getApplicationContext(), "Se ha generado un error", Toast.LENGTH_LONG).show();
                                            }

                                        }


                                    }

                                    @Override
                                    public void onFailure(Call<Usuario> call, Throwable t) {
                                        progreso.dismiss();
                                        Toast.makeText(getApplicationContext(), "Se ha generado un error", Toast.LENGTH_LONG).show();

                                    }

                                });

                            }
                        }

                        @Override
                        public void onFailure(Call<Usuario> call, Throwable t) {
                            progreso.dismiss();
                            Toast.makeText(getApplicationContext(), "Se ha generado un error", Toast.LENGTH_LONG).show();

                        }
                    });

                } catch (Exception e) {
                    progreso.dismiss();
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    private JsonObject getJsonFromObject(Usuario usuario) {
        JsonObject usuarioJson = new JsonObject();
        usuarioJson.addProperty("name", usuario.getNombre());
        usuarioJson.addProperty("lastname", usuario.getApellido());
        usuarioJson.addProperty("email", usuario.getEmail());
        usuarioJson.addProperty("zone", usuario.getZona());
        usuarioJson.addProperty("age", usuario.getEdad());
        usuarioJson.addProperty("password", usuario.getContrasenia());
        return usuarioJson;
    }

    static String contraseniaRandom(int n)
    {


        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";


        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {


            int index
                    = (int)(AlphaNumericString.length()
                    * Math.random());


            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }
}
