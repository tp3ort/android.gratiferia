package com.example.gratiferia;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gratiferia.Helpers.Publicacion;
import com.example.gratiferia.Helpers.PublicacionesService;
import com.example.gratiferia.Helpers.RecyclerViewAdapter;
import com.example.gratiferia.Helpers.RetrofitService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ConsumirPublicacionesActivity extends AppCompatActivity {


    RecyclerView rw;

    RecyclerViewAdapter rwa;

    ProgressDialog progreso;

    ArrayList<Publicacion> publicacionesObtenidas;

    ArrayList<Publicacion> publicacionesToCards;

    View viewMyLayout;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {



        publicacionesToCards = new ArrayList<>();

        super.onCreate(savedInstanceState);



        Intent i = getIntent();

        setContentView(R.layout.activity_consumir_publicaciones);




        rw = findViewById(R.id.recyclerViewPubli);

        popularAdapter(i);
    }

    public void popularAdapter(Intent i) {


        LayoutInflater inflater = LayoutInflater.from(ConsumirPublicacionesActivity.this); // or (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        viewMyLayout = inflater.inflate(R.layout.activity_loading, null);
        viewMyLayout.setVisibility(View.VISIBLE);

        progreso = new ProgressDialog(ConsumirPublicacionesActivity.this);
        progreso.setMessage("Cargando publicaciones...");
        progreso.setTitle("Publicaciones");
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.show();

        Retrofit retrofit = RetrofitService.retrofit();

        PublicacionesService pubService = retrofit.create(PublicacionesService.class);

        Call<ArrayList<Publicacion>> call;

        if (i.getExtras()!=null){
            String idUsuario = i.getExtras().get("usuarioId").toString();
            if (!idUsuario.equals("")){
                call = pubService.getPublicacionesByOwner(idUsuario);
            }else{
                call = pubService.getPublicaciones();
            }
        }else{
            call = pubService.getPublicaciones();
        }



        call.enqueue(new Callback<ArrayList<Publicacion>>() {
            @Override
            public void onResponse(Call<ArrayList<Publicacion>> call, Response<ArrayList<Publicacion>> response) {

                viewMyLayout.setVisibility(View.GONE);

                publicacionesObtenidas = response.body();

                setPublicacionesToCards();

                initAdapter();

                initScrollListener();

                progreso.dismiss();


            }

            @Override
            public void onFailure(Call<ArrayList<Publicacion>> call, Throwable t) {
                viewMyLayout.setVisibility(View.GONE);
                progreso.dismiss();
                Toast.makeText(ConsumirPublicacionesActivity.this, "Ha ocurrido un error", Toast.LENGTH_LONG).show();
            }
        });
    }



    private void initAdapter (){

        rwa = new RecyclerViewAdapter(ConsumirPublicacionesActivity.this,publicacionesToCards);
        rw.setLayoutManager(new GridLayoutManager(ConsumirPublicacionesActivity.this, 1));
        rw.setAdapter(rwa);

    }

    boolean isLoading = false;

    private void initScrollListener() {
        rw.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                GridLayoutManager linearGridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearGridLayoutManager != null && linearGridLayoutManager.findLastCompletelyVisibleItemPosition() == publicacionesToCards.size() - 1) {
                        loadMore();
                        isLoading = true;
                    }
                }
            }
        });


    }

    private void setPublicacionesToCards() {

        int i = 0;

        final int MAXIMO_PUBLICACIONES = 3;

        if (publicacionesObtenidas.size()==0 || publicacionesObtenidas == null){
            // manejar sin resultados
        }else{
            while(i<MAXIMO_PUBLICACIONES && publicacionesToCards.size() < publicacionesObtenidas.size()) {

                publicacionesToCards.add(publicacionesObtenidas.get(publicacionesToCards.size()));
                i++;
            }
        }


    }

    private void loadMore() {
        publicacionesToCards.add(null);
        rwa.notifyItemInserted(publicacionesToCards.size() - 1);


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                publicacionesToCards.remove(publicacionesToCards.size() - 1);

                setPublicacionesToCards();

                rwa.notifyDataSetChanged();
                isLoading = false;
            }
        }, 2000);


    }


}
