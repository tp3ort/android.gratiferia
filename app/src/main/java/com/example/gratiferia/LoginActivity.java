package com.example.gratiferia;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.gratiferia.Helpers.RetrofitService;
import com.example.gratiferia.Helpers.Usuario;
import com.example.gratiferia.Helpers.UsuarioService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    Button btnIniciar;
    TextView olvidoContrasenia;
    ProgressDialog progreso;

    SharedPreferences pref;

    EditText txtEmail, txtContrasenia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        pref = getSharedPreferences("user_details",MODE_PRIVATE);

        btnIniciar = findViewById(R.id.btnRecPass);
        txtEmail = findViewById(R.id.txtRecEmail);
        txtContrasenia = findViewById(R.id.txtContrasenia);

        olvidoContrasenia = findViewById(R.id.txtOlvidoPass);

        olvidoContrasenia.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RecuperarContraseniaActivity.class);
                startActivity(intent);
            }
        });


        btnIniciar.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {

                try {

                    progreso = new ProgressDialog(LoginActivity.this);
                    progreso.setMessage("Validando...");
                    progreso.setTitle("Ingreso");
                    progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);



                    Retrofit retrofit = RetrofitService.retrofit();

                    UsuarioService jsonPlaceHolderApi = retrofit.create(UsuarioService.class);
                    Call<Usuario> call = jsonPlaceHolderApi.getUsuario(txtEmail.getText().toString());
                    progreso.show();
                    progreso.setCancelable(false);
                    call.enqueue(new Callback<Usuario>() {
                        @Override
                        public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                            if (response.code() == 400){
                                progreso.dismiss();
                                Toast.makeText(getApplicationContext(),"El email es inválido", Toast.LENGTH_LONG).show();
                            }else if (response.code() == 404){
                                progreso.dismiss();
                                Toast.makeText(getApplicationContext(),"El email ingresado no existe", Toast.LENGTH_LONG).show();
                            }
                            else{

                                Usuario usuario = response.body();
                                String pass = txtContrasenia.getText().toString();
                                String us = txtEmail.getText().toString();

                                progreso.dismiss();

                                if (usuario.getContrasenia().equals(pass)
                                        && usuario.getEmail().equals(us)){

                                    setUsuarioEnSesion(usuario);

                                    Intent intent = new Intent(LoginActivity.this,MenuPrincipalActivity.class);
                                    startActivity(intent);
                                    finish();

                                }else{
                                    progreso.dismiss();
                                    Toast.makeText(getApplicationContext(),"Email y/o Contraseña inválidos", Toast.LENGTH_LONG).show();

                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Usuario> call, Throwable t) {
                            progreso.dismiss();
                            Toast.makeText(getApplicationContext(), "Se ha generado un error", Toast.LENGTH_LONG).show();

                        }
                    });

                } catch (Exception e) {
                    progreso.dismiss();
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    private void setUsuarioEnSesion(Usuario usuario) {

        SharedPreferences.Editor editor = pref.edit();
        editor.putString("nombre",usuario.getNombre());
        editor.putString("apellido",usuario.getApellido());
        editor.putString("email",usuario.getEmail());
        editor.putInt("edad",usuario.getEdad());
        editor.putString("zona",usuario.getZona());
        editor.putString("contrasenia",usuario.getContrasenia());
        editor.putString("id",usuario.getId());
        editor.commit();
    }
}
