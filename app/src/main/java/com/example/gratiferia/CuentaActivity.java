package com.example.gratiferia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Layout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.gratiferia.Helpers.Connection;
import com.example.gratiferia.Helpers.RetrofitService;
import com.example.gratiferia.Helpers.Usuario;
import com.example.gratiferia.Helpers.UsuarioService;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class CuentaActivity extends AppCompatActivity implements LocalizacionDialog.LocalizacionDialogListener {


    Button btnActualizarCuenta;
    EditText txtNombre, txtApellido, txtEdad, txtZona, txtEmail, txtContrasenia;

    Connection con;

    ProgressDialog progreso;

    SharedPreferences prf;


    Usuario usuario = new Usuario();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta);

        con = new Connection(this);

        if (!con.getIsOnline()){
            con.informarSinConexion(findViewById(android.R.id.content)  ,0, "Sin conexión a internet");

        }


        prf = getSharedPreferences("user_details",MODE_PRIVATE);
        btnActualizarCuenta = findViewById(R.id.btnActualizarCuenta);


        setUsuarioFromSesion(prf);

        txtZona.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b){

                    LocalizacionDialog localizacionDialog = new LocalizacionDialog();
                    localizacionDialog.show(getSupportFragmentManager(), "dialog");
                    view.clearFocus();
                }

            }
        });

        btnActualizarCuenta.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {


                try {

                    progreso = new ProgressDialog(CuentaActivity.this);
                    progreso.setMessage("Validando datos ingresados...");
                    progreso.setTitle("Actualizacion de Cuenta");
                    progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);


                    Retrofit retrofit = RetrofitService.retrofit();

                    JsonObject usuarioJson = getJsonFromInputs();



                    UsuarioService jsonPlaceHolderApi = retrofit.create(UsuarioService.class);
                    Call<Usuario> call = jsonPlaceHolderApi.updateUsuario(usuario.getId() ,usuarioJson);
                    progreso.show();
                    progreso.setCancelable(false);
                    call.enqueue(new Callback<Usuario>() {
                        @Override
                        public void onResponse(Call<Usuario> call, final Response<Usuario> response) {
                            if (response.code() == 400){


                                progreso.dismiss();

                                try {
                                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                                    Toast.makeText(getApplicationContext(), jObjError.getString("descripcion") ,Toast.LENGTH_LONG).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                            }
                            else{

                                if (response.isSuccessful()){
                                    progreso.dismiss();


                                    Toast.makeText(getApplicationContext(), "Datos actualizados correctamente", Toast.LENGTH_LONG).show();
                                    new Thread(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            try
                                            {
                                                usuario = response.body();
                                                UpdateUsuarioInSesion(usuario);
                                                Thread.sleep(1000);

                                                finish();
                                            }
                                            catch (InterruptedException e)
                                            {
                                                // TODO Auto-generated catch block
                                                e.printStackTrace();
                                            }
                                        }
                                    }).start();

                                }else{
                                    progreso.dismiss();
                                    Toast.makeText(getApplicationContext(), "Se ha generado un error", Toast.LENGTH_LONG).show();
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<Usuario> call, Throwable t) {
                            progreso.dismiss();
                            Toast.makeText(getApplicationContext(), "Se ha generado un error", Toast.LENGTH_LONG).show();

                        }
                    });

                } catch (Exception e) {
                    progreso.dismiss();
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }




        });



    }

    private void setInputsFromUsuario(Usuario usuario) {

        txtNombre = findViewById(R.id.txtNombre);
        txtNombre.setText(usuario.getNombre());

        txtApellido = findViewById(R.id.txtApellido);
        txtApellido.setText(usuario.getApellido());

        txtEdad = findViewById(R.id.txtEdad);
        txtEdad.setText(String.valueOf(usuario.getEdad()));

        txtEmail = findViewById(R.id.txtRecEmail);
        txtEmail.setText(usuario.getEmail());

        txtZona = findViewById(R.id.txtZona);
        txtZona.setText(usuario.getZona());

        txtContrasenia = findViewById(R.id.txtContrasenia);
        txtContrasenia.setText(usuario.getContrasenia());



    }

    private void setUsuarioFromSesion(SharedPreferences sesion) {

        usuario.setNombre(sesion.getString("nombre",null));
        usuario.setApellido(sesion.getString("apellido",null));
        usuario.setEdad(sesion.getInt("edad",0));
        usuario.setZona(sesion.getString("zona",null));
        usuario.setEmail(sesion.getString("email",null));
        usuario.setId(sesion.getString("id",null));
        usuario.setContrasenia(sesion.getString("contrasenia",null));

        setInputsFromUsuario(usuario);


    }

    private void UpdateUsuarioInSesion(Usuario updatedUsuario) {

        SharedPreferences.Editor editor = prf.edit();
        editor.putString("nombre",updatedUsuario.getNombre());
        editor.putString("apellido",updatedUsuario.getApellido());
        editor.putString("email",updatedUsuario.getEmail());
        editor.putInt("edad",updatedUsuario.getEdad());
        editor.putString("zona",updatedUsuario.getZona());
        editor.putString("contrasenia",updatedUsuario.getContrasenia());
        editor.commit();


    }

    private JsonObject getJsonFromInputs(){


        String nombre = txtNombre.getText().toString();
        String apellido = txtApellido.getText().toString();
        String email = txtEmail.getText().toString();
        String contrasenia = txtContrasenia.getText().toString();
        String zona = txtZona.getText().toString();
        String strEdad = txtEdad.getText().toString();

        if (nombre == null || nombre.isEmpty()){
            throw new IllegalArgumentException("Debe indicar su Nombre");
        }else if (apellido == null || apellido.isEmpty()){
            throw new IllegalArgumentException("Debe indicar su Apellido");
        }else if (strEdad == null || strEdad.isEmpty()){
            throw new IllegalArgumentException("Debe indicar su Edad");
        }else if (zona == null || zona.isEmpty()){
            throw new IllegalArgumentException("Debe indicar una Zona");
        }else if (email == null || email.isEmpty()){
            throw new IllegalArgumentException("Debe indicar un Email");
        }else if (contrasenia == null || contrasenia.isEmpty()){
            throw new IllegalArgumentException("Debe indicar una Contraseña");
        }

        JsonObject usuarioJson = new JsonObject();
        usuarioJson.addProperty("name", nombre);
        usuarioJson.addProperty("lastname", apellido);
        usuarioJson.addProperty("email", email);
        usuarioJson.addProperty("zone", zona);
        usuarioJson.addProperty("age", Integer.parseInt(strEdad));
        usuarioJson.addProperty("password", contrasenia);


        return usuarioJson;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){

        getMenuInflater().inflate(R.menu.menu_principal, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accion_cuenta:
                Intent intent = new Intent(CuentaActivity.this, CuentaActivity.class);
                startActivity(intent);
                return true;

            case R.id.accion_buscar:
                // User chose the "Favorite" action, mark the current item
                Toast.makeText(this,"Boton Buscar", Toast.LENGTH_LONG).show();
                return true;


            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void aplicarCampos(String returned, LocalizacionDialog.Coordenada coordenada) {
        txtZona.setText(returned);
    }



}
