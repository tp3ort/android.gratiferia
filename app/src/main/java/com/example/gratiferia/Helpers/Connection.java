package com.example.gratiferia.Helpers;

import android.content.Context;
import android.net.ConnectivityManager;

import android.net.NetworkInfo;
import android.view.View;
import android.widget.SearchView;


import com.google.android.material.snackbar.Snackbar;

import static androidx.core.content.ContextCompat.getSystemService;

public class Connection {

    ConnectivityManager connectivityManager;

    boolean isOnline;

    public Connection(Context context){

        connectivityManager = getSystemService(context,ConnectivityManager.class);
        isOnline = getIsOnline();
    }



    public boolean getIsOnline(){
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            return true;
        }else{
            return false;
        }
    }

    public void informarSinConexion(View mView, int duracion, String mensaje){
        Snackbar.make(mView, "Sin acceso a internet", duracion).show();

    }



}
