package com.example.gratiferia.Helpers;

import com.google.gson.JsonObject;

import org.junit.runners.Parameterized;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UsuarioService {

    @GET("usuarios/{email}")
    Call<Usuario> getUsuario(@Path("email") String email);

  /*  @Headers("Content-Type: application/json")
    @POST("usuarios")
    Call<Usuario> createUsuario(@Body Usuario usuario);
    */

    @Headers("Content-Type: application/json")
    @POST("usuarios")
    Call<Usuario> createUsuario(@Body JsonObject usuario);

    @Headers("Content-Type: application/json")
    @PUT("usuarios/{id}")
    Call<Usuario> updateUsuario(@Path("id") String id, @Body JsonObject usuario);

    final String TOKEN = "393c8bf9a655816a42ca0f02791ba231c860bdd3548d3e9f85e513dc0610f6244d153e494bef649961a638c260d1d62de03d66a5c1aeaab549b25389512aa1a3";

    @POST("/v1/send?access_token="+TOKEN)
    Call<String> sendMail(@Body JsonObject mail);



}
