package com.example.gratiferia.Helpers;

import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface LocalizacionService {


    @Headers("Content-Type: application/json")
    @POST("publicaciones")
    Call<Publicacion> createPublicacion(@Body JsonObject publicacion);

    @GET("geocode.json")
    Call<JsonObject> getLocalizaciones(
            @Query("housenumber") String altura,
            @Query("street") String calle,
            @Query("city") String localidad,
            @Query("country") String pais,
            @Query("gen") String gen,
            @Query("app_id") String app_id,
            @Query("app_code") String app_code

            );

}
