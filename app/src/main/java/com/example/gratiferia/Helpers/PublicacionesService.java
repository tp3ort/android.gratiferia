package com.example.gratiferia.Helpers;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PublicacionesService {

        String API_ROUTE = "publicaciones";

        @GET(API_ROUTE)
        Call<ArrayList<Publicacion>> getPublicaciones();

        @Headers("Content-Type: application/json")
        @POST("publicaciones")
        Call<Publicacion> createPublicacion(@Body JsonObject publicacion);

        @GET("publicaciones/")
        Call<ArrayList<Publicacion>> getPublicacionesByOwner(@Query("owner") String idUsuario);

}
