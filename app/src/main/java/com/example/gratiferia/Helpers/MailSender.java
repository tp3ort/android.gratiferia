package com.example.gratiferia.Helpers;
import com.mailjet.client.errors.MailjetException;
import com.mailjet.client.errors.MailjetSocketTimeoutException;
import com.mailjet.client.MailjetClient;
import com.mailjet.client.MailjetRequest;
import com.mailjet.client.MailjetResponse;
import com.mailjet.client.ClientOptions;
import com.mailjet.client.resource.Emailv31;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
public class MailSender {

    public static void sendMailRegistro(Usuario usuario) throws MailjetException, MailjetSocketTimeoutException, JSONException {
        MailjetClient client;
        MailjetRequest request;
        MailjetResponse response;
        client = new MailjetClient("23e5ca2416bed5a7da665168e2fb8834", "779e277d7c4b770a3a2ca3f85c89a0c9", new ClientOptions("v3.1"));
        request = new MailjetRequest(Emailv31.resource)
                .property(Emailv31.MESSAGES, new JSONArray()
                        .put(new JSONObject()
                                .put(Emailv31.Message.FROM, new JSONObject()
                                        .put("Email", "gestionapport@gmail.com")
                                        .put("Name", "Gratiferia"))
                                .put(Emailv31.Message.TO, new JSONArray()
                                        .put(new JSONObject()
                                                .put("Email", usuario.getEmail())
                                                .put("Name", usuario.getNombre()))).put(Emailv31.Message.TEMPLATEID, 1050244)
                                .put(Emailv31.Message.TEMPLATELANGUAGE, true)
                                .put(Emailv31.Message.SUBJECT, "Bienvenid@ a Gratiferia")
                                .put(Emailv31.Message.VARIABLES, new JSONObject()
                                        .put("nombre", usuario.getNombre()))));
        response = client.post(request);
        System.out.println(response.getStatus());
        System.out.println(response.getData());
    }

    public static void sendMailRecupero(Usuario usuario) throws MailjetException, MailjetSocketTimeoutException, JSONException {
        MailjetClient client;
        MailjetRequest request;
        MailjetResponse response;
        client = new MailjetClient("23e5ca2416bed5a7da665168e2fb8834", "779e277d7c4b770a3a2ca3f85c89a0c9", new ClientOptions("v3.1"));
        request = new MailjetRequest(Emailv31.resource)
                .property(Emailv31.MESSAGES, new JSONArray()
                        .put(new JSONObject()
                                .put(Emailv31.Message.FROM, new JSONObject()
                                        .put("Email", "gestionapport@gmail.com")
                                        .put("Name", "Gratiferia"))
                                .put(Emailv31.Message.TO, new JSONArray()
                                        .put(new JSONObject()
                                                .put("Email", usuario.getEmail())
                                                .put("Name", usuario.getNombre()))).put(Emailv31.Message.TEMPLATEID, 1050251)
                                .put(Emailv31.Message.TEMPLATELANGUAGE, true)
                                .put(Emailv31.Message.SUBJECT, "Recupero de contraseña")
                                .put(Emailv31.Message.VARIABLES, new JSONObject()
                                        .put("nombre", usuario.getNombre()).put("nuevapass", usuario.getContrasenia()))));
        response = client.post(request);
        System.out.println(response.getStatus());
        System.out.println(response.getData());
    }
}
