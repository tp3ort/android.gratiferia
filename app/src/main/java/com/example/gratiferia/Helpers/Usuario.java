package com.example.gratiferia.Helpers;
import android.content.SharedPreferences;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Usuario {



    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String nombre;
    @SerializedName("lastname")
    private String apellido;
    @SerializedName("age")
    private int edad;
    @SerializedName("email")
    private String email;
    @SerializedName("zone")
    private String zona;
    @SerializedName("password")
    private String contrasenia;


    public Usuario(String nombre, String apellido, String email, int edad, String zona, String contrasenia){
        setNombre(nombre);
        setApellido(apellido);
        setEmail(email);
        setEdad(edad);
        setZona(zona);
        setContrasenia(contrasenia);
    }
    public Usuario(){

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        if (nombre == null || nombre.isEmpty())
            throw new IllegalArgumentException("El nombre no puede estar vacío");
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        if (apellido == null || apellido.isEmpty())
            throw new IllegalArgumentException("El apellido no puede estar vacío");
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        if (edad <= 0 || edad > 125)
            throw new IllegalArgumentException("Rango de edad no válido");
        this.edad = edad;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if (email == null || email.isEmpty())
            throw new IllegalArgumentException("El email no puede estar vacío");
        this.email = email;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        if (contrasenia == null || contrasenia.isEmpty())
            throw new IllegalArgumentException("La contraseña no puede estar vacía");
        this.contrasenia = contrasenia;
    }


}
