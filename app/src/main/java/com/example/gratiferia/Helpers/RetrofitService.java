package com.example.gratiferia.Helpers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitService {



    private static OkHttpClient okHttpClient(){

        OkHttpClient httpClient = new OkHttpClient.Builder()
                .connectTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(90, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .build();

        return httpClient;
    }

    public static Retrofit retrofit(){

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder().
                baseUrl("https://apirestgratiferia.herokuapp.com/api/").client(RetrofitService.okHttpClient()).
                addConverterFactory(GsonConverterFactory.create(gson)).
                build();

        return retrofit;
    }

    public static Retrofit hereApi(){

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit hereApi = new Retrofit.Builder().
                baseUrl("https://geocoder.api.here.com/6.2/").client(RetrofitService.okHttpClient()).
                addConverterFactory(GsonConverterFactory.create(gson)).
                build();

        return hereApi;

    }
}
