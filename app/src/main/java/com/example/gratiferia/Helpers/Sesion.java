package com.example.gratiferia.Helpers;

import android.content.Context;
import android.content.SharedPreferences;

public class Sesion {

    Context context;
    SharedPreferences sharedPreferences;

    public void quitarSesion(){
        sharedPreferences.edit().clear().commit();
    }

    public String getSesion() {
        sesion = sharedPreferences.getString("usuarioSesion","");
        return sesion;
    }

    public void setSesion(String sesion) {

        this.sesion = sesion;
        sharedPreferences.edit().putString("usuarioSesion", sesion).commit();
    }

    String sesion;

    public Sesion(Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences("usuarioInfo", Context.MODE_PRIVATE);
    }

}
