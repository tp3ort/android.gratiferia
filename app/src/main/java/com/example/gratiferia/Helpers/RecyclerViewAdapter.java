package com.example.gratiferia.Helpers;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gratiferia.R;
import com.example.gratiferia.VerPublicacionActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;


    private Context mContext;
    private List<Publicacion> mData;

    public RecyclerViewAdapter (Context mContext, List<Publicacion> mData){
        this.mContext = mContext;
        this.mData = mData;
    }



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {



        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardviewitem, parent, false);
            return new mViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof mViewHolder) {

            populateItemRows((mViewHolder) holder, position);
        } else if (holder instanceof LoadingViewHolder) {
            showLoadingView((LoadingViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    public int getItemViewType(int position) {
        return mData.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }


    private void showLoadingView(LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }
    private class mViewHolder extends RecyclerView.ViewHolder{

            TextView titulo, descripcion, zona, categoria, estado;
            String latitud="", longitud="";
            ImageView imagen;
            String pathImagen;
            ProgressBar progressBar;

            public mViewHolder(View itemView){
                super(itemView);

                progressBar = itemView.findViewById(R.id.progressBarCards);

                titulo = itemView.findViewById(R.id.txtTituloPublicacion);
                descripcion = (TextView) itemView.findViewById(R.id.txtDescripcionPublicacion);
                zona = (TextView) itemView.findViewById(R.id.txtZonaPublicacion);
                categoria = (TextView) itemView.findViewById(R.id.txtCategoriaPublicacion);
                estado = (TextView) itemView.findViewById(R.id.txtEstadoPublicacion);
                imagen = (ImageView) itemView.findViewById(R.id.imgPubli);


                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i = new Intent(mContext, VerPublicacionActivity.class);


/*
                        Bitmap bitmap = ((BitmapDrawable)imagen.getDrawable()).getBitmap();
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("imagen",bitmap);
*/

                       // i.putExtras(bundle);

                        i.putExtra("imagen", pathImagen);
                        i.putExtra("titulo", titulo.getText());
                        i.putExtra("descripcion", descripcion.getText());
                        i.putExtra("categoria", categoria.getText());
                        i.putExtra("estado", estado.getText());
                        i.putExtra("zona", zona.getText());
                        i.putExtra("zonalat", latitud);
                        i.putExtra("zonalong", longitud);
                        try{
                            mContext.startActivity(i);
                        }catch(Exception e){
                            Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                        }




                    }
                });

            }


    }

    private void populateItemRows(final mViewHolder holder, int position) {



        holder.titulo.setText(mData.get(position).getTitle());
        holder.descripcion.setText(mData.get(position).getDescription());
        holder.zona.setText(mData.get(position).getZone());
        holder.categoria.setText(mData.get(position).getCategory());
        holder.estado.setText(mData.get(position).getState());
        holder.latitud = mData.get(position).getZoneLat();
        holder.longitud = mData.get(position).getZoneLong();
        Picasso.get().load(mData.get(position).getImage()).resize(200,200)
                .rotate(90).centerCrop()
                .into(holder.imagen, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                });

        holder.pathImagen = mData.get(position).getImage();

        //.centerCrop() y rezize()
    }

}
