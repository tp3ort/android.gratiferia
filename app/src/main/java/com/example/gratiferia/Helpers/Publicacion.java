package com.example.gratiferia.Helpers;

import com.google.gson.annotations.SerializedName;

public class Publicacion {

    public Publicacion() {
    }

    @SerializedName("id")
    private String pubId;
    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("category")
    private String category;
    @SerializedName("zone")
    private String zone;
    @SerializedName("zonelat")
    private String zonelat;
    @SerializedName("zonelong")
    private String zonelong;
    @SerializedName("keyword")
    private String keyword;
    @SerializedName("state")
    private String state;
    @SerializedName("owner")
    private String owner; //ver si hacemos una clase user
    @SerializedName("reservedby")
    private String reservedby;
    @SerializedName("image")
    private String image;

    public String getPubId() {
        return pubId;
    }

    public void setPubId(String pubId) {
        this.pubId = pubId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getReservedby() {
        return reservedby;
    }

    public void setReservedby(String reservedby) {
        this.reservedby = reservedby;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getZoneLat() {
        return zonelat;
    }

    public void setZoneLat(String zonelat) {
        this.zonelat = zonelat;
    }

    public String getZoneLong() {
        return zonelong;
    }

    public void setZoneLong(String zonelong) {
        this.zonelong = zonelong;
    }

}
