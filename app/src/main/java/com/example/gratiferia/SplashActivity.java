package com.example.gratiferia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;


import android.content.Context;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.material.snackbar.Snackbar;

public class SplashActivity extends AppCompatActivity {


    ProgressBar spinner;

    ConnectivityManager connectivityManager;

    SharedPreferences sharedPreferences;

    ConstraintLayout constraintLayout;

    private long ms = 0, tiempoSplash= 1200;

    private boolean splashActivo = true, pausado = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        spinner = findViewById(R.id.spinnerSplash);
        spinner.setVisibility(View.GONE);
        spinner.setVisibility(View.VISIBLE);

        // MODO Pantalla completa --- no queda muy bien ---

/*        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);*/

        // Verificacion de Conexión a Internet
        connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);


        // Verificación de Sesión

        sharedPreferences = getSharedPreferences("user_details",MODE_PRIVATE);

        constraintLayout = findViewById(R.id.constLay);


        Thread hilo = new Thread(){

            public void run(){

                try {

                        while (splashActivo && ms < tiempoSplash){
                            if (!pausado){
                                ms = ms+100;
                                sleep(100);
                            }
                        }

                }catch (Exception e){

                }finally {

                    if (!isOnline()){
                      informarSinConexion();
                    }else{

                        if (isLogin()){
                            abrirMenuPrincipal();
                        }else{
                            abrirMain();
                        }
                    }


                }

            }

        };

        hilo.start();


    }




    private boolean isLogin() {

        String mail = sharedPreferences.getString("email", "");

            if (!mail.equals("")){
                return true;
            }else{
                return false;
            }

    }

    private boolean isOnline(){
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            return true;
        }else{
            return false;
        }
    }

    private void informarSinConexion(){
        Snackbar snackbar = Snackbar
                .make(constraintLayout, "Sin acceso a internet", Snackbar.LENGTH_INDEFINITE)
                .setAction("Reintentar", new View.OnClickListener(){


                    @Override
                    public void onClick(View view) {
                        recreate();

                    }
                });
        snackbar.show();
    }


    private void abrirMain(){
        Intent intent = new Intent(SplashActivity.this, MainActivity.class );
        startActivity(intent);
        finish();
    }

    private void abrirMenuPrincipal(){
        Intent intent = new Intent(SplashActivity.this, MenuPrincipalActivity.class );
        startActivity(intent);
        finish();


    }
}
