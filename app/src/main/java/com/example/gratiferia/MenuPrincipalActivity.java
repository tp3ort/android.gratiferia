package com.example.gratiferia;


import androidx.appcompat.app.AppCompatActivity;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MenuPrincipalActivity extends AppCompatActivity {

    Button btnCuenta;
    Button btnBuscar;
    Button btnNueva;
    Button btnSalir;
    Button btnMisPublicaciones;

    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);



        sharedPreferences = getSharedPreferences("user_details",MODE_PRIVATE);


        btnCuenta = findViewById(R.id.btnMiCuenta);
        btnCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuPrincipalActivity.this, CuentaActivity.class);
                startActivity(intent);

            }
        });

        btnBuscar = findViewById(R.id.btnBuscarPublicacion);
        btnBuscar.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {


            }
        });

        btnMisPublicaciones = findViewById(R.id.btnMisPublicaciones);
        btnMisPublicaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuPrincipalActivity.this, ConsumirPublicacionesActivity.class);
                intent.putExtra("usuarioId", sharedPreferences.getString("id",""));
                startActivity(intent);
            }
        });

        btnNueva = findViewById(R.id.btnNuevaPublicacion);
        btnNueva.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MenuPrincipalActivity.this, CrearPublicacionActivity.class);
                startActivity(intent);

            }
        });

        btnSalir = findViewById(R.id.btnSalir);
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new AlertDialog.Builder(MenuPrincipalActivity.this)
                        .setTitle("Cerrar sesión")
                        .setMessage("Gracias por gratiferiar. Hasta la próxima "+sharedPreferences.getString("nombre", ""))
                        .setPositiveButton("Cerrar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                cerrarSesion();
                                Intent intent = new Intent(MenuPrincipalActivity.this, SplashActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        })
                        .create().show();



            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){

        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }

    private void cerrarSesion(){

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("nombre","");
        editor.putString("apellido","");
        editor.putString("email","");
        editor.putInt("edad",0);
        editor.putString("zona","");
        editor.putString("contrasenia","");
        editor.putString("id","");
        sharedPreferences.edit().clear().commit();
        editor.commit();


    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accion_cuenta:
                Intent intent = new Intent(MenuPrincipalActivity.this, CuentaActivity.class);
                startActivity(intent);
                return true;

            case R.id.accion_buscar:
                // User chose the "Favorite" action, mark the current item
                Toast.makeText(this,"Boton Buscar", Toast.LENGTH_LONG).show();
                return true;


            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}
