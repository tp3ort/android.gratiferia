package com.example.gratiferia;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.gratiferia.Helpers.MailSender;
import com.example.gratiferia.Helpers.RetrofitService;
import com.example.gratiferia.Helpers.Usuario;
import com.example.gratiferia.Helpers.UsuarioService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.mailjet.client.errors.MailjetException;
import com.mailjet.client.errors.MailjetSocketTimeoutException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RegistroActivity extends AppCompatActivity implements LocalizacionDialog.LocalizacionDialogListener {

    Button btnRegistro;
    EditText txtNombre, txtApellido, txtEdad, txtZona, txtEmail, txtContrasenia;
    Usuario usuario;
    ProgressDialog progreso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        setCampos();

        btnRegistro = findViewById(R.id.btnCrearCuenta);

        txtZona.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b){

                    LocalizacionDialog localizacionDialog = new LocalizacionDialog();
                    localizacionDialog.show(getSupportFragmentManager(), "dialog");
                }

            }
        });

        btnRegistro.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {



                try {

                    progreso = new ProgressDialog(RegistroActivity.this);
                    progreso.setMessage("Validando datos ingresados...");
                    progreso.setTitle("Registro");
                    progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);

                    Retrofit retrofit = RetrofitService.retrofit();

                    JsonObject usuarioJson = getJsonFromInputs();

                    UsuarioService jsonPlaceHolderApi = retrofit.create(UsuarioService.class);
                    Call<Usuario> call = jsonPlaceHolderApi.createUsuario(usuarioJson);
                    progreso.show();
                    progreso.setCancelable(false);
                    call.enqueue(new Callback<Usuario>() {
                        @Override
                        public void onResponse(Call<Usuario> call, final Response<Usuario> response) {
                            if (response.code() == 400){

                                progreso.dismiss();

                                try {
                                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                                    Toast.makeText(getApplicationContext(), jObjError.getString("descripcion") ,Toast.LENGTH_LONG).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                            }
                            else{

                                if (response.isSuccessful()){
                                    usuario = response.body();
                                    progreso.dismiss();
                                    limpiarCampos();

                                    Toast.makeText(getApplicationContext(), "Registro Exitoso. Por favor Inicie Sesión.", Toast.LENGTH_LONG).show();
                                    new Thread(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            try
                                            {
                                                MailSender.sendMailRegistro(usuario);
                                                Thread.sleep(2000);

                                                finish();
                                            }
                                            catch (InterruptedException e)
                                            {
                                                // TODO Auto-generated catch block
                                                e.printStackTrace();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            } catch (MailjetSocketTimeoutException e) {
                                                e.printStackTrace();
                                            } catch (MailjetException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }).start();

                                }else{
                                    progreso.dismiss();
                                    Toast.makeText(getApplicationContext(), "Se ha generado un error", Toast.LENGTH_LONG).show();
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<Usuario> call, Throwable t) {
                            progreso.dismiss();
                            Toast.makeText(getApplicationContext(), "Se ha generado un error", Toast.LENGTH_LONG).show();

                        }
                    });

                } catch (Exception e) {
                    progreso.dismiss();
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }




        });




    }

    private void setCampos() {


        txtNombre = findViewById(R.id.txt_Nombre);
        txtApellido = findViewById(R.id.txt_Apellido);
        txtEdad = findViewById(R.id.txt_Edad);
        txtEmail = findViewById(R.id.txt_Email);
        txtZona = findViewById(R.id.txt_Zona);
        txtContrasenia = findViewById(R.id.txt_Contraseña);
    }


    private void limpiarCampos() {

        txtNombre.setText("");
        txtApellido.setText("");
        txtEdad.setText("");
        txtEmail.setText("");
        txtZona.setText("");
        txtContrasenia.setText("");
    }

    private JsonObject getJsonFromInputs(){

        JsonObject usuario = new JsonObject();

        String nombre = txtNombre.getText().toString();
        String apellido = txtApellido.getText().toString();
        String email = txtEmail.getText().toString();
        String contrasenia = txtContrasenia.getText().toString();
        String zona = txtZona.getText().toString();
        String strEdad = txtEdad.getText().toString();

        if (nombre == null || nombre.isEmpty()){
            throw new IllegalArgumentException("Debe indicar su Nombre");
        }else if (apellido == null || apellido.isEmpty()){
            throw new IllegalArgumentException("Debe indicar su Apellido");
        }else if (strEdad == null || strEdad.isEmpty()){
            throw new IllegalArgumentException("Debe indicar su Edad");
        }else if (zona == null || zona.isEmpty()){
            throw new IllegalArgumentException("Debe indicar una Zona");
        }else if (email == null || email.isEmpty()){
            throw new IllegalArgumentException("Debe indicar un Email");
        }else if (contrasenia == null || contrasenia.isEmpty()){
            throw new IllegalArgumentException("Debe indicar una Contraseña");
        }

        JsonObject usuarioJson = new JsonObject();
        usuarioJson.addProperty("name", nombre);
        usuarioJson.addProperty("lastname", apellido);
        usuarioJson.addProperty("email", email);
        usuarioJson.addProperty("zone", zona);
        usuarioJson.addProperty("age", Integer.parseInt(strEdad));
        usuarioJson.addProperty("password", contrasenia);


        return usuarioJson;

    }


    @Override
    public void aplicarCampos(String returned, LocalizacionDialog.Coordenada coordenada) {
        txtZona.setText(returned);
    }
}
